function Station(coordinates, isIntersection = false) {
  this.x = coordinates.x;
  this.y = coordinates.y;
  this.isIntersection = isIntersection;
  this.isOccupied = false;
  this.init = function() {
    this.draw();
  };
  this.draw = function() {
    let pointSize = 3; // Change according to the size of the point.
    let ctx = document.getElementById("canvas").getContext("2d");
    ctx.fillStyle = "#ff00ff"; // Red color
    if (this.isIntersection) {
      ctx.fillStyle = "#ff2626"; // Red color
      pointSize = 6;
    }
    ctx.beginPath(); //Start path
    ctx.arc(this.x, this.y, pointSize, 0, Math.PI * 2, true); // Draw a point using the arc function of the canvas with a point structure.
    ctx.fill(); // Close the path and fill.
  }
  this.init();
}

function RailRoad(points, numStations) {
  this.points = points;
  this.stations = [];
  this.numStations = numStations;
  this.distance = Math.sqrt(Math.pow((this.points.p1x - this.points.p2x), 2) + Math.pow((this.points.p1y - this.points.p2y), 2));
  this.init = function() {
    this.draw();
  };
  this.draw = function() {
    let ctx = document.getElementById("canvas").getContext("2d");
    ctx.beginPath();
    ctx.moveTo(this.points.p1x, this.points.p1y);
    ctx.lineTo(this.points.p2x, this.points.p2y);
    ctx.stroke();
  }
  this.init();
}

function Train(numPassengers, start, end) {
  this.passengers = numPassengers;
  this.start = start;
  this.end = end;
  this.currentPosition = start;
  this.nextPosition = undefined;
  this.wait = false;
  this.direction = 1;
  this.speed = 1;

  this.move = function() {
    if (!this.wait) {

      var tx = this.end.x - this.start.x;
      let ty = this.end.y - this.start.y;
      let dist = Math.sqrt(tx * tx + ty * ty);
      let rad = Math.atan2(ty, tx);
      let angle = rad / Math.PI * 180;
      let velX = (tx / dist) * this.speed;
      let velY = (ty / dist) * this.speed;
      if (dist > 1) {
        //let start = this.end;
        //let end = this.start;
        //this.start = start;
        //this.end = end;
        this.currentPosition.x += velX;
        this.currentPosition.y += velY;
      } else if (dist < 0.1) {
        console.log("arrived")
        let start = this.end;
        let end = this.start;
        this.start = start;
        this.end = end;
      }

      this.draw();
    }
  }
  this.init = function() {
    console.log('Hi! I have  ' + this.passengers + ' passengers!');
    this.draw();
  }
  this.draw = function() {
    let pointSize = 4; // Change according to the size of the point.
    let ctx = document.getElementById("canvas").getContext("2d");
    ctx.fillStyle = "#000000"; // Red color
    ctx.beginPath(); //Start path
    ctx.arc(this.currentPosition.x, this.currentPosition.y, pointSize, 0, Math.PI * 2, true); // Draw a point using the arc function of the canvas with a point structure.
    ctx.fill(); // Close the path and fill.
  }
  this.init();

}
//TODO: rename points to parameters
function RailRoadController(points) {
  this.points = points;
  this.railroads = [];
  this.stations = [];

  this.init = function() {
    this.generateRailroads();
    this.addTrains();
    setInterval(this.moveTrains.bind(this), 10);
  }
  this.generateRailroads = function() {
    let _this = this;
    for (var i = 0; i < this.points.length; i++) {
      let railroad = new RailRoad(this.points[i], 6);
      _this.stations.push(new Station({
        x: points[i].p1x,
        y: points[i].p1y
      }, false));
      _this.stations.push(new Station({
        x: points[i].p2x,
        y: points[i].p2y
      }, false));
      this.railroads.push({
        railroad: railroad,
        stations: [
          _this.stations[_this.stations.length - 2],
          _this.stations[_this.stations.length - 1]
        ],
        passengers: this.points[i].passengers
      });
    }
    //Finding railroads intersections
    this.railroads.pairs(function(pair) {
      let intersect = _this.intersect(pair[0].railroad.points.p1x, pair[0].railroad.points.p1y, pair[0].railroad.points.p2x, pair[0].railroad.points.p2y, pair[1].railroad.points.p1x, pair[1].railroad.points.p1y, pair[1].railroad.points.p2x, pair[1].railroad.points.p2y);
      _this.stations.push(new Station(intersect, true));
      pair[0].stations.push(_this.stations[_this.stations.length - 1]);
      pair[1].stations.push(_this.stations[_this.stations.length - 1]);
    });

    //ordering stations by coordinates
    for (var i = 0; i < this.railroads.length; i++) {
      this.railroads[i].stations.sort(function(a, b) {
        if (a.x == b.x) {
          return a.y - b.y;
        }
        return a.x - b.x || a.y - b.y;
      });
    }

  }
  this.addTrains = function() {

    for (var i = 0; i < this.railroads.length; i++) {
      this.railroads[i].train = new Train(this.railroads[i].passengers, this.railroads[i].stations[0], this.railroads[i].stations[this.railroads[i].stations.length - 1]);
    }
    console.log(this.railroads)
  }

  this.moveTrains = function() {
    let ctx = document.getElementById("canvas").getContext("2d");
    ctx.fillStyle = "#fff";
    ctx.clearRect(0, 0, 1000, 1000);
    for (var i = 0; i < this.stations.length; i++) {
      this.stations[i].draw();
    }

    for (var i = 0; i < this.railroads.length; i++) {
      this.railroads[i].railroad.draw();
      this.railroads[i].train.move();
    }

  }

  this.intersect = function(x1, y1, x2, y2, x3, y3, x4, y4) {
    // Check if none of the lines are of length 0
    if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) {
      return false
    }
    denominator = ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1))
    // Lines are parallel
    if (denominator === 0) {
      return false
    }
    let ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator
    let ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator
    // is the intersection along the segments
    if (ua < 0 || ua > 1 || ub < 0 || ub > 1) {
      return false
    }
    // Return a object with the x and y coordinates of the intersection
    let x = x1 + ua * (x2 - x1)
    let y = y1 + ua * (y2 - y1)
    return {x, y}
  }
  this.init();
}

Array.prototype.pairs = function(func) {
  for (let i = 0; i < this.length - 1; i++) {
    for (let j = i; j < this.length - 1; j++) {
      func([
        this[i],
        this[j + 1]
      ]);
    }
  }
}

let railRoadsParams = [
  {
    p1x: 100,
    p1y: 100,
    p2x: 300,
    p2y: 400,
    passengers: 80
  }, {
    p1x: 300,
    p1y: 100,
    p2x: 200,
    p2y: 400,
    passengers: 20
  }, {
    p1x: 50,
    p1y: 200,
    p2x: 450,
    p2y: 150,
    passengers: 100
  }
];

var railRoadController = new RailRoadController(railRoadsParams);
